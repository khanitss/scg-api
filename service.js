const express = require("express")
const bodyParser = require("body-parser")
const mysql = require("mysql")
var cors = require('cors') /////// block

var multer = require('multer')
var uuid = require('uuid'); //random ตัวอักษรและตัวเลข เพื่อมาทำเปนชื่อไฟล์ที่อัพโหลด
var nameImg = ""
var storage = multer.diskStorage({
    destination: './public/images',
    filename:function(req,file,cb){
        let extension = file.originalname.split(".").pop();
        cb(null, uuid.v1()+'.'+extension);
        nameImg = uuid.v1()+'.'+extension
    }
});
var upload = multer({
    storage: storage
})
const dbConn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'db_scg' //// database
})
const app = express();
app.use(cors())
app.use(express.json());
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(express.static('public'));

app.post('/upload',upload.single('img'),function(req,res,next){
    console.log(upload.single('img'));
    console.log(req.body.img);
    console.log(nameImg)
    res.sendStatus(200);
})

////////////////////////////////////////////////////////////

app.get('/', (req, res) => {
    res.send('<h1>Hello</h1>');
});

app.post('/addProduct',upload.single('img'),(req,res)=>{
    var p_name = req.body.p_name
    var p_price = req.body.p_price
    var p_detail = req.body.p_detail
    var p_img = nameImg
   
    dbConn.query('INSERT INTO product (p_name, p_price, p_status, p_detail, p_img) VALUES (?, ?, 1, ?, ?)',
        [p_name,p_price,p_detail,p_img],(error,results)=>{
        if(error){
            res.send("400")
        }else{
            res.send("200")
        }
    })
})

app.get('/GetAllProductThisMachine',(req,res)=>{ /////ถ้าเป็นเครื่องอื่น m_id ก็เปลี่ยนไปตามเครื่อง
    dbConn.query('select * from stock inner join product on stock.p_id = product.p_id where m_id = 1',[],(error, results)=>{
        res.send(results)
    })
})

app.post('/buyProduct',(req,res)=>{
    var p_id = req.body.p_id
    var m_id = 1
    var amount = 1
    var total = 0
   
    dbConn.query('select * from stock where m_id = ? and p_id = ?',[m_id, p_id],(error, results)=>{
        if(results[0].amount == 0){
            res.send("Out of Stock")
        }else{
            total = results[0].amount - amount
            dbConn.query('update stock set amount = ? where m_id = ? and p_id = ?',[total, m_id, p_id],(error, results)=>{
                if(error){
                    res.send("400")
                }else{
                    res.send("200")
                }
            })
        }  
    })
})

app.get('/GetAllMachine',(req,res)=>{
    dbConn.query('select * from machine',[],(error, results)=>{
        res.send(results)
    })
})

app.get('/GetAllBranch',(req,res)=>{
    dbConn.query('select * from branch',[],(error, results)=>{
        res.send(results)
    })
})

app.get('/GetAllStaff',(req,res)=>{
    var data = []
    dbConn.query('select * from staff INNER JOIN branch ON staff.b_id = branch.b_id',[],(error, results)=>{
        
        for(let i=0;i<results.length;i++){
            data.push({
                staff_id:results[i].staff_id,
                staff_name:results[i].staff_fname + " " + results[i].staff_lname,
                staff_username:results[i].staff_username,
                staff_password:results[i].staff_password,
                staff_status:results[i].staff_status,
                b_name:results[i].b_name
            });
        }
        //console.log(data)
        //res.send(data)
        res.json(data)
    })
})

app.post('/getProductById',(req,res)=>{
    var p_id = req.body.p_id
    console.log(p_id)
    dbConn.query('select * FROM product WHERE p_id = ?',[p_id],(error, results)=>{
        res.send(results)
    })
})

app.post('/getStaffById',(req,res)=>{
    var staff_id = req.body.staff_id
    console.log(staff_id)
    var dataStaff = [];
    dbConn.query('SELECT * FROM staff INNER JOIN branch ON staff.b_id = branch.b_id where staff_id = ?',[staff_id],(error, results)=>{
        for(let i = 0; i<results.length; i++){
            dataStaff.push({
                staff_id:results[i].staff_id,
                staff_name:results[i].staff_fname + " " + results[i].staff_lname,
                staff_username:results[i].staff_username,
                staff_password:results[i].staff_password,
                staff_status:results[i].staff_status,
                b_name:results[i].b_name
            });
        }
        res.send(dataStaff)
    })
})

app.post('/addReqProduct',(req,res)=>{
    var p_id = req.body.p_id
    var m_id = req.body.m_id
    var req_amount = req.body.req_amount
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;

    console.log(p_id)
    console.log(m_id)
    console.log(req_amount)
    console.log(dateTime)
    dbConn.query('INSERT INTO req_product (p_id, m_id, req_amount, req_status, req_date_time) VALUES (?, ?, ?, 1, ?)',[p_id,m_id,req_amount,dateTime],(error,results)=>{
        if(error){
            res.send("400")
            
        }else{
            res.send("200")
        }
    })
})

app.post('/getReqByM_idandB_id',(req,res)=>{
    dbConn.query('select * FROM req_product WHERE m_id = ? order by req_date_time desc',[req.body.m_id, req.body.p_id],(error, results)=>{
        res.send(results)
    })
})

app.get('/getAllReq',(req,res)=>{
    dbConn.query('select * FROM req_product where req_status != 0 order by req_status asc',[],(error, results)=>{
        res.send(results)
    })
})

app.get('/getReqNotApproved',(req,res)=>{
    dbConn.query('select * FROM req_product WHERE req_status = 0',[],(error, results)=>{
        res.send(results)
    })
})

app.get('/getReqWaitApprove',(req,res)=>{
    dbConn.query('select * FROM req_product WHERE req_status = 1',[],(error, results)=>{
        res.send(results)
    })
})

app.get('/getReqApproved',(req,res)=>{
    dbConn.query('select * FROM req_product WHERE req_status = 2',[],(error, results)=>{
        res.send(results)
    })
})

app.post('/ApproveReq',(req,res)=>{
    var req_id = req.body.req_id
    var dataInputToTran = []
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;
    dbConn.query('update req_product set req_status = 2 where req_id= ?',[req_id],(error,results)=>{
        if(error){
            res.send("400")
        }else{
            dbConn.query('select * from req_product where req_id = ?',[req_id],(error,results)=>{
                dataInputToTran.push({
                    m_id:results[0].m_id,
                    p_id:results[0].p_id,
                    amount:results[0].req_amount,
                    date_time:dateTime
                })
                dbConn.query('INSERT INTO stock_tran (m_id, p_id, amount, st_status, st_date_time) VALUES (?, ?, ?, 1, ?)',
                    [dataInputToTran[0].m_id, dataInputToTran[0].p_id, dataInputToTran[0].amount, dataInputToTran[0].date_time],(error,results)=>{
                    if(error){
                        res.send("add tran error")
                    }else{
                        res.send("add tran success")
                    }
                })           
            }) 
        }
    })
})

app.post('/CancelReq',(req,res)=>{
    var req_id = req.body.req_id
    dbConn.query('update req_product set req_status = 0 where req_id= ?',[req_id],(error,results)=>{
        if(error){
            res.send("400")
        }else{
            res.send("200")
        }
    })
})

app.get('/getAllTran',(req,res)=>{
    dbConn.query('select * FROM stock_tran where st_status != 0 order by st_status asc',[],(error, results)=>{
        res.send(results)
    })
})

app.post('/updateStockTran',(req,res)=>{
    var st_id = req.body.st_id
    var st_status = req.body.st_status
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;
    dbConn.query('update stock_tran set st_status = ? where st_id = ?',[st_status,st_id],(error,results)=>{
        if(error){
            res.send("400")
        }else{ 
            dbConn.query('select * from stock_tran where st_id = ?',[st_id],(error,results)=>{
                var data_stock_tran = []
                data_stock_tran.push({
                    m_id:results[0].m_id,
                    p_id:results[0].p_id,
                    amount:results[0].amount,
                    status:results[0].st_status
                })
                console.log(data_stock_tran[0].status)
                if(data_stock_tran[0].status == 3){
                    dbConn.query('select * from stock where m_id = ? And p_id = ?',[data_stock_tran[0].m_id, data_stock_tran[0].p_id],(error,results)=>{
                        //console.log(results)
                        if(results == ""){
                            dbConn.query('INSERT INTO stock (m_id, p_id, amount, update_date_time) VALUES (?, ?, ?, ?)',
                                [data_stock_tran[0].m_id, data_stock_tran[0].p_id, data_stock_tran[0].amount, dateTime],(error,results)=>{
                                if(error){
                                    res.send("add stock failed")
                                }else{
                                    res.send("add stock success")
                                }
                            })
                        }else{
                            let a = results[0].amount
                            let b = data_stock_tran[0].amount
                            let c = a + b
                            dbConn.query('update stock set amount = ?, update_date_time = ? where m_id = ? And p_id = ?',
                                [c, dateTime, data_stock_tran[0].m_id, data_stock_tran[0].p_id],(error,results)=>{
                                if(error){
                                    res.send("update stock failed")
                                }else{
                                    res.send("update stock success")
                                }
                            })
                        }
                    })    
                }else{
                    res.send("update stock tran status")
                }
            })
        }
    })
})

app.post('/login',(req,res)=>{
    var username = req.body.username
    var password = req.body.password
    dbConn.query('select * from staff where staff_username = ? and staff_password = ?',[username,password],(error,results)=>{
        if(error){
            res.send("400")
        }else{
            if(results == ""){
                res.send("username หรือ password ไม่ถูกต้อง")
            }else{
                res.send(results)
            } 
        }
    })
})

app.post('/getMachineByBid',(req,res)=>{
    var b_id = req.body.b_id
    
    dbConn.query('select * from machine where b_id = ?',[b_id],(error,results)=>{
        if(error){
            res.send("400")
        }else{
            res.send(results)
        }
    })
})

app.post('/getStockByMid',(req,res)=>{
    dbConn.query('select * from stock INNER JOIN product ON stock.p_id = product.p_id where m_id = ?',[req.body.m_id],(error,results)=>{
        if(error){
            res.send("400")
        }else{
            res.send(results)
        }
    })
})

app.get('/getStockByMid1',(req,res)=>{
    dbConn.query('select * from stock INNER JOIN product ON stock.p_id = product.p_id where m_id = 1',[],(error,results)=>{
        if(error){
            res.send("400")
        }else{
            res.send(results)
        }
    })
})

app.get('/getStockByMid2',(req,res)=>{
    dbConn.query('select * from stock INNER JOIN product ON stock.p_id = product.p_id where m_id = 2',[],(error,results)=>{
        if(error){
            res.send("400")
        }else{
            res.send(results)
        }
    })
})


app.get('/getStockbeingSoldout',(req,res)=>{
    
    dbConn.query('select * from stock INNER JOIN product ON stock.p_id = product.p_id where amount < 10',[],(error,results)=>{
        if(error){
            res.send("400")
        }else{
            res.send(results)
        }
    })
})


const port = process.env.PORT || 3300
app.listen(port, () => console.log(`Listening on port${port}...`) );