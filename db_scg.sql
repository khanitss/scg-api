-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 31, 2021 at 07:47 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_scg`
--

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `b_id` int(11) NOT NULL,
  `b_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `b_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`b_id`, `b_name`, `b_status`) VALUES
(1, 'สำนักงานใหญ่', 1),
(2, 'ขอนแก่น', 1);

-- --------------------------------------------------------

--
-- Table structure for table `machine`
--

CREATE TABLE `machine` (
  `m_id` int(11) NOT NULL,
  `m_lat` varchar(50) COLLATE utf8_bin NOT NULL,
  `m_long` varchar(50) COLLATE utf8_bin NOT NULL,
  `m_status` int(11) NOT NULL,
  `b_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `machine`
--

INSERT INTO `machine` (`m_id`, `m_lat`, `m_long`, `m_status`, `b_id`) VALUES
(1, '16.428861', '102.848305', 1, 2),
(2, '16.436612', '102.848131', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `p_id` int(11) NOT NULL,
  `p_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `p_price` float NOT NULL,
  `p_status` int(11) NOT NULL,
  `p_detail` varchar(1000) COLLATE utf8_bin NOT NULL,
  `p_img` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`p_id`, `p_name`, `p_price`, `p_status`, `p_detail`, `p_img`) VALUES
(3, 'Grape Juice', 69, 1, 'Grape juice is obtained from crushing and blending grapes into a liquid. In the wine industry, grape juice that contains 7–23 percent of pulp, skins, stems and seeds is often referred to as must. The sugars in grape juice allow it to be used as a sweetener, and fermented and made into wine, brandy, ', 'grape-single.png'),
(5, 'Lemon Juice', 69, 1, 'The juice of the lemon is about 5% to 6% citric acid, with a pH of around 2.2, giving it a sour taste. The distinctive sour taste of lemon juice makes it a key ingredient in drinks and foods such as lemonade and lemon meringue pie.', 'lemon-single.png'),
(6, 'Orange Juice', 69, 1, 'On a molecular level, orange juice is composed of organic acids, sugars, and phenolic compounds. The main organic acids found in orange juice are citric, malic, and ascorbic acid. The major sugars found in orange juice are sucrose, glucose, and fructose.', 'orange-single.png'),
(7, 'Strawberry Juice', 69, 1, 'Strawberry juice is the fruit juice from strawberries. It is rich with Vitamin C and ranges from sweet to sweet tart in taste. The fragrantly sweet juiciness and deep red color of strawberries can brighten up both the taste and aesthetics of any meal; it is no wonder they are the most popular berry fruit in the world.', 'strawberry-single.png');

-- --------------------------------------------------------

--
-- Table structure for table `req_product`
--

CREATE TABLE `req_product` (
  `req_id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `m_id` int(11) NOT NULL,
  `req_amount` int(11) NOT NULL,
  `req_status` int(11) NOT NULL,
  `req_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `req_product`
--

INSERT INTO `req_product` (`req_id`, `p_id`, `m_id`, `req_amount`, `req_status`, `req_date_time`) VALUES
(7, 3, 1, 50, 2, '2021-01-31 23:41:33'),
(8, 3, 1, 10, 2, '2021-02-01 01:25:23');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `staff_id` int(11) NOT NULL,
  `staff_fname` varchar(100) COLLATE utf8_bin NOT NULL,
  `staff_lname` varchar(100) COLLATE utf8_bin NOT NULL,
  `staff_username` varchar(100) COLLATE utf8_bin NOT NULL,
  `staff_password` varchar(100) COLLATE utf8_bin NOT NULL,
  `staff_status` int(11) NOT NULL,
  `b_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `staff_fname`, `staff_lname`, `staff_username`, `staff_password`, `staff_status`, `b_id`) VALUES
(1, 'admin', 'administator', 'admin', '123456', 1, 1),
(2, 'ผู้จัดการ', 'สาขาขอนแก่น', 'manager', '123456', 2, 2),
(3, 'พนักงาน', 'ขนส่งสินค้า', 'driver', '123456', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `m_id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `update_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`m_id`, `p_id`, `amount`, `update_date_time`) VALUES
(1, 3, 4, '2021-01-31 23:42:41'),
(1, 5, 4, '2021-01-31 22:35:56'),
(1, 6, 20, '2021-01-31 22:36:38'),
(1, 7, 20, '2021-01-31 22:36:38');

-- --------------------------------------------------------

--
-- Table structure for table `stock_tran`
--

CREATE TABLE `stock_tran` (
  `st_id` int(11) NOT NULL,
  `m_id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `st_status` int(11) NOT NULL,
  `st_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `stock_tran`
--

INSERT INTO `stock_tran` (`st_id`, `m_id`, `p_id`, `amount`, `st_status`, `st_date_time`) VALUES
(4, 1, 3, 50, 3, '2021-01-31 23:41:53'),
(5, 1, 3, 10, 0, '2021-02-01 01:29:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`b_id`);

--
-- Indexes for table `machine`
--
ALTER TABLE `machine`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `req_product`
--
ALTER TABLE `req_product`
  ADD PRIMARY KEY (`req_id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `stock_tran`
--
ALTER TABLE `stock_tran`
  ADD PRIMARY KEY (`st_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `b_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `machine`
--
ALTER TABLE `machine`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `req_product`
--
ALTER TABLE `req_product`
  MODIFY `req_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `stock_tran`
--
ALTER TABLE `stock_tran`
  MODIFY `st_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
